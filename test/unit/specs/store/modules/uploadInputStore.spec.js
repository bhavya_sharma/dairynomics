import uploadInputStore from '@/store/modules/uploadInputStore'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

describe('mutations for uploadInputStore module', () => {
  const state = {
    loading: false,
    error: '',
    currentComponent: 'ProductSection',
    selectedInputs: [],
    Inputs: []
  }

  it('calls the SAVE_INPUTS mutation', () => {
    uploadInputStore.mutations.SAVE_INPUTS(state, [])
    expect(state.selectedInputs).toEqual([])
  })

  it('calls the SUBMIT_INPUTS_LOADING mutation', () => {
    uploadInputStore.mutations.SUBMIT_INPUTS_LOADING(state)
    expect(state.loading).toEqual(true)
  })

  it('calls the SUBMIT_INPUTS_SUCCESS mutation', () => {
    uploadInputStore.mutations.SUBMIT_INPUTS_SUCCESS(state, 'CELLFAM')
    expect(state.Inputs).toEqual('CELLFAM')
    expect(state.loading).toEqual(false)
  })

  it('calls the SUBMIT_INPUTS_ERROR mutation', () => {
    uploadInputStore.mutations.SUBMIT_INPUTS_ERROR(state, 'CELLFAM')
    expect(state.error).toEqual('CELLFAM')
    expect(state.loading).toEqual(false)
  })

  it('calls the CHANGE_COMPONENT mutation', () => {
    uploadInputStore.mutations.CHANGE_COMPONENT(state, 'UploadInput')
    expect(state.currentComponent).toEqual('UploadInput')
  })

  describe('Getters for uploadInputStore module', () => {
    const state = {
      loading: false,
      error: '',
      currentComponent: 'ProductSection',
      selectedInputs: [],
      Inputs: []
    }

    it('tests the uploadInputDetails getters', () => {
      const uploadInputDetails = uploadInputStore.getters.uploadInputDetails(
        state
      )
      expect(uploadInputDetails).toEqual(state)
    })
  })

  describe('Actions for uploadInputStore module', () => {
    afterEach(() => {
      mock.reset()
    })
    // const router = {
    //   push: jest.fn()
    // }

    // it('successfully submits inputs from the API', () => {
    //   const commit = jest.fn()
    //   const response = {
    //     data: {
    //       success: true,
    //       message: 'success',
    //       products: [1, 2, 3]
    //     }
    //   }
    //   mock
    //     .onGet('https://beta.cowsoko.com/api/v1/users/null/products')
    //     .reply(200, response)
    //   uploadProductStore.actions.fetchMyProducts({ commit }).then(() => {
    //     expect(commit).toHaveBeenCalled()
    //   })
    // })

    //   it('it throws error when fetching user products from the API', () => {
    //     const commit = jest.fn()
    //     const response = {
    //       success: false
    //     }
    //     mock
    //       .onGet('https://beta.cowsoko.com/api/v1/users/null/products')
    //       .reply(400, response)
    //     uploadProductStore.actions.fetchMyProducts({ commit }).then(() => {
    //       expect(commit).toHaveBeenCalled()
    //     })
    //   })

    it('successfully submit user inputs to the API', () => {
      const commit = jest.fn()
      const dispatch = jest.fn()
      const context = { commit, dispatch }
      const formData = 'cellfam'
      mock.onPost('https://beta.cowsoko.com/api/v1/inputs').reply(201)
      uploadInputStore.actions.submitInputs(context, formData).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('throws an error when trying to submit user inputs to the API', () => {
      const commit = jest.fn()
      const dispatch = jest.fn()
      const context = { commit, dispatch }
      const formData = 'cellfam'
      mock.onPost('https://beta.cowsoko.com/api/v1/inputs').reply(400)
      uploadInputStore.actions.submitInputs(context, formData).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })
  })
})
