import Vuex from 'vuex'
import { mount, createLocalVue } from '@vue/test-utils'
import LandingPage from '@/pages/LandingPage/LandingPage.vue'
import AppButton from '@/components/ui/AppButton.vue'
import DairyOptions from '@/pages/LandingPage/DairyOptions'
import landingPage from '@/store/modules/landingPage'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('@components/LandingPage.vue', () => {
  const mockedRouter = { push: jest.fn() }
  const store = new Vuex.Store({
    modules: { landingPage }
  })

  const wrapper = mount(LandingPage, {
    beforeCreate () {
      this.$router = mockedRouter
    },
    localVue,
    store
  })

  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<h1 class="landing-page-title">Welcome to Dairynomics</h1>')
    expect(wrapper.html()).toContain('<h5 class="dairy-options-introduction">Let\'s first get to know you ...</h5>')
    expect(wrapper.vm.disabled).toBe(true)
  })

  it('contains AppButton component', () => {
    expect(wrapper.contains(AppButton)).toBe(true)
    expect(wrapper.findAll(AppButton)).toHaveLength(1)
  })

  it('contains DairyOptions component', () => {
    expect(wrapper.contains(DairyOptions)).toBe(true)
    expect(wrapper.findAll(DairyOptions)).toHaveLength(4)
  })

  it('should select a dairyoption when @handleDairyOptionClick happens and enable button', () => {
    wrapper.findAll(DairyOptions).at(1).vm.$emit('handleDairyOptionClick')
    expect(wrapper.vm.disabled).toBe(false)
    expect(wrapper.findAll(DairyOptions).at(1).vm.isSelected).toBe(true)
  })

  it('Should have "$router" defined and matching expected mock.', () => {
    expect(wrapper.vm.$router).toEqual(mockedRouter)
  })

  it('should route to a new URL', () => {
    wrapper.findAll(DairyOptions).at(0).vm.$emit('handleDairyOptionClick')
    wrapper.find(AppButton).vm.$emit('click')
    expect(mockedRouter.push).toHaveBeenCalledTimes(1)
  })

  it('should select a dairy option and enable button if store has a user type', () => {
    landingPage.state.userType = 'I am a dairy farmer'
    const wrapper = mount(LandingPage, { localVue, store })

    expect(wrapper.vm.disabled).toBe(false)
    expect(wrapper.findAll(DairyOptions).at(0).vm.isSelected).toBe(true)
  })
})
