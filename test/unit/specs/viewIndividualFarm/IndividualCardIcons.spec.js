import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualCardIcons from '@/pages/ViewIndividualFarm/IndividualCardIcons'
import mockStore from '@/store/modules/mocks/IndividualFarmStore'

const localVue = createLocalVue()

localVue.use(Vuex)

jest.mock('@/store')

describe('Individual Card Icon.vue', () => {
  const wrapper = shallowMount(IndividualCardIcons, {store: mockStore, localVue})

  it('should render without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
