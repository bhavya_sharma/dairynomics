import UserTestsModule, { userTests } from '@/store/modules/UserTestsModule'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

router.push = jest.fn()

jest.mock('@/store')

jest.mock('@/router/router')

describe('mutation for user tests module', () => {
  const state = {
    test: [],
    randomTest: {},
    userTests,
    userTestResults: userTests
  }
  it('calls the SET_USER_TESTS mutation', () => {
    UserTestsModule.mutations.SET_USER_TEST(state, [{ key: 'value' }])
    expect(state.test).toEqual([{ key: 'value' }])
  })
  it('calls the SET_RANDOM_USER_TEST mutation', () => {
    UserTestsModule.mutations.SET_RANDOM_USER_TEST(state, {key: 'value'})
    expect(state.randomTest).toBeTruthy()
  })
  it('calls the SET_USER_TEST_RESULTS mutation', () => {
    UserTestsModule.mutations.SET_USER_TEST_RESULTS(state, {key: 'value'})
    expect(state.userTestResults).toEqual({key: 'value'})
  })
})

describe('Getters for user tests module', () => {
  const state = {
    tests: [],
    randomTest: {},
    userTests,
    userTestResults: []
  }
  it('tests the USER_TESTS getter', () => {
    const tests = UserTestsModule.getters.ALL_USER_TESTS(state)
    expect(tests).toEqual(state.userTests)
  })

  it('tests the RANDOM_USER_TEST getter', () => {
    const test = UserTestsModule.getters.RANDOM_USER_TEST(state)
    expect(test).toEqual(state.randomTest)
  })

  it('tests the ALL_USER_RESULTS getter', () => {
    const test = UserTestsModule.getters.ALL_USER_RESULTS(state)
    expect(test).toEqual(state.userTestResults)
  })

  it('tests the SORTED_TEST_RESULTS getter when no results are returned', () => {
    const test = UserTestsModule.getters.SORTED_TEST_RESULTS(state)
    expect(test[userTests[0].title]).toBeUndefined()
    expect(test[userTests[1].title]).toBeUndefined()
    expect(test[userTests[2].title]).toBeUndefined()
  })
  it('tests that the SORTED_TEST_RESULTS getter sorts by date when results are returned', () => {
    var today = new Date()
    var yesterday = new Date(today)
    yesterday.setDate(today.getDate() - 1)

    const userTestResults = [{ test_title: 'Herds Composition Test', created_at: { date: today }, results: '45' }, { test_title: 'Herds Composition Test', created_at: { date: yesterday }, results: '85' }]
    const test = UserTestsModule.getters.SORTED_TEST_RESULTS({...state, userTestResults})
    expect(test['Herds Composition']).toBeUndefined()
  })

  it('tests the ALL_USER_TESTS getter', () => {
    const test = UserTestsModule.getters.ALL_USER_TESTS(state)
    expect(test).toEqual(state.userTests)
  })
})

describe('Actions for user tests module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('successfully fetches recommended tests from the API', () => {
    const commit = jest.fn()
    const response = []
    mock.onGet('https://beta.cowsoko.com/api/v1/onboarding_test').reply(200, response)
    UserTestsModule.actions.GET_USER_TESTS({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should return a 404 page when an error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/onboarding_test').reply(404, response)
    UserTestsModule.actions.GET_USER_TESTS({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should redirect to home page when an unknown error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/onboarding_test').reply(400, response)
    UserTestsModule.actions.GET_USER_TESTS({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })

  it('GET_USER_TEST_RESULTS successfully fetches user\'s results from the API', () => {
    const commit = jest.fn()
    const mockId = '3303'
    const response = {
      data: {
        onboarding_results: []
      }
    }
    mock.onGet(`https://beta.cowsoko.com/api/v1/user_test/${mockId}`).reply(200, response)
    UserTestsModule.actions.GET_USER_TEST_RESULTS({commit}, mockId).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should redirect to home page when an unknown error occurs', () => {
    const commit = jest.fn()
    const mockId = '3303'
    mock.onGet(`https://beta.cowsoko.com/api/v1/user_test/${mockId}`).reply(404)
    UserTestsModule.actions.GET_USER_TEST_RESULTS({commit}, mockId).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should call router.push when the statuscode is not 404', () => {
    const commit = jest.fn()
    const mockId = '3305'
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet(`https://beta.cowsoko.com/api/v1/user_test/${mockId}`).reply(400, response)

    UserTestsModule.actions.GET_USER_TEST_RESULTS({ commit }, mockId).then(() => {
      expect(router.push).toHaveBeenCalledwith('/')
    })
  })
})
