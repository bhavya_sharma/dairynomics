import AxiosCalls from '@/utils/api/AxiosCalls'
import router from '@/router/router'
import {
  alertError,
  findCounty,
  getAPIErrorMessage
} from '@/utils/helperFunctions'

export default {
  state: {
    counties: [],
    selected: { county: 'All-Counties', id: 0 },
    experts: {},
    isLoading: false,
    currentPageNumber: 0,
    defaultCounty: { county: 'All-Counties', id: 0 }
  },

  getters: {
    currentPage (state) {
      const stateVal = state.experts
      if (stateVal && stateVal[state.selected.id]) {
        return stateVal[state.selected.id][state.currentPageNumber]
      }
    },
    expertListIsCached: state => (countyId, pageNumber) => {
      return state.experts[countyId] && state.experts[countyId][pageNumber]
    }
  },
  actions: {
    async getCounties ({ commit, state }) {
      if (!state.counties.length) {
        const { counties, error } = await AxiosCalls.awaitGet('counties')
        if (error) return alertError(getAPIErrorMessage(error))
        commit('SET_COUNTIES', counties)
      }
    },

    async getExpertCounties ({ commit, state, dispatch }, paramsCounty) {
      commit('TOGGLE_IS_LOADING', true)
      let selectedCounty
      await dispatch('getCounties')
      selectedCounty = findCounty(state.counties, paramsCounty)
      if (!selectedCounty) router.push(router.history.current.fullPath)
      const page = router.history.current.query.page
      dispatch('getExperts', { selectedCounty, pageNumber: page })
    },

    async getExperts (
      { commit, state, getters },
      { selectedCounty = state.defaultCounty, pageNumber = 1 }
    ) {
      commit('SET_SELECTED_COUNTY', selectedCounty)
      commit('TOGGLE_IS_LOADING', true)
      const urlPath = selectedCounty.id
        ? `experts/county/${selectedCounty.id}?page=${pageNumber}`
        : `experts?page=${pageNumber}`
      const data = await AxiosCalls.awaitGet(urlPath)
      if (data.error) {
        if (data.error !== 401) alertError(getAPIErrorMessage(data.error))
        return
      }
      commit('SET_CURRENT_PAGE', data.meta.current_page)
      commit('UPDATE_EXPERTS', { data, selectedCounty })
      commit('TOGGLE_IS_LOADING', false)
    }
  },
  mutations: {
    UPDATE_EXPERTS (state, data) {
      const pageNo = state.currentPageNumber
      const county = data.selectedCounty.id
      let expertData = {}
      expertData[pageNo] = data.data
      state.experts[county] = expertData
    },
    TOGGLE_IS_LOADING (state, payload) {
      state.isLoading = payload
    },
    SET_COUNTIES (state, payload) {
      state.counties = [state.selected, ...payload]
    },
    SET_SELECTED_COUNTY (state, payload) {
      state.selected = payload
    },
    SET_CURRENT_PAGE (state, payload) {
      state.currentPageNumber = Number(payload)
    }
  }
}
