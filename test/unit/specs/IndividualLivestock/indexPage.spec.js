import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import IndividualLivestockIndexPage from '@/pages/IndividualLivestock/index.vue'

jest.mock('@/store')
// jest.mock('@/router/router')

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Router)
const router = new Router()

describe('IndividualLivestockIndexPage', () => {
  let wrapper
  let store
  let getters
  let actions
  beforeEach(() => {
    getters = {
      avalaibleliveStock: () => 'value_1'
    }
    actions = {
      getIndividualLivestock: () => 'value_1'
    }
    store = new Vuex.Store({ getters, actions })
  })

  it('should render the IndividualLivestockIndexPage component', () => {
    wrapper = shallowMount(IndividualLivestockIndexPage, { data: {
      activeSwitcherLink: 'veiwLivestock',
      switcherOptionList: [
        {
          optionName: 'Veiw Livestock',
          linkName: 'veiwLivestock'
        },
        {
          optionName: 'Similar Livestock',
          linkName: 'similarLivestock'
        }
      ]
    },
    store,
    localVue,
    router
    })
    expect(wrapper.vm).toBeTruthy()
  })

  it('should trigger the activeSwitcher method', () => {
    wrapper = shallowMount(IndividualLivestockIndexPage, { data: {
      activeSwitcherLink: 'veiwLivestock'},
    store,
    localVue,
    router
    })
    wrapper.vm.activeSwitcher('veiw')
    expect(wrapper.vm.activeSwitcherLink).toBe('veiw')
  })
})
