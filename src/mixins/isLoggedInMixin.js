import { authenticateUser } from '@/utils/helperFunctions'

export default {
  computed: {
    isLoggedIn () {
      return authenticateUser().isValid
    }
  }
}
