import signupStoreModules from '@/store/modules/signupStoreModules'
import {mock as mocked} from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

jest.mock('@/router/router')

const userDetails = {
  first_name: 'dairynomics',
  last_name: 'cellfarm',
  phone: '070377754927',
  county: 'baringo',
  password: '123456',
  account_type: 'i am a dairy farmer',
  country_code: '+256'
}

describe('signupStoreModules', () => {
  //   MUTATIONS
  it('toggles loader to true or false', () => {
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }}
    // apply mutation
    signupStoreModules.mutations.LOADER(state, true)
    // assert result
    expect(state.userAuthSignUp.loading).toEqual(true)
  })

  it('changes state base on the payload', () => {
    // mock state
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }}
    const payload = {
      data: {
        id: 5
      }
    }

    signupStoreModules.mutations.SIGNED_UP_USER(state, payload)
    expect(state.userAuthSignUp.userId).toEqual(5)
    expect(state.userAuthSignUp.success).toEqual(true)
  })

  it('changes state base on the payload', () => {
    // mock state
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }}
    const payload = {
      user: {
        id: 5
      }
    }

    signupStoreModules.mutations.SIGNED_UP_USER(state, payload)
    expect(state.userAuthSignUp.userId).toEqual(5)
    expect(state.userAuthSignUp.success).toEqual(true)
  })

  it('should update the success property to false', () => {
    // mock state
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }}
    const payload = {
      error: '422',
      message: 'error message'
    }

    signupStoreModules.mutations.SIGNED_UP_USER(state, payload)
    expect(state.userAuthSignUp.success).toEqual(false)
  })

  // signup actions
  describe('userSignup actions', () => {
    afterEach(() => {
      mocked.reset()
    })

    it('test action to signup a user', () => {
      const commit = jest.fn()

      const resp = {
        data: []
      }
      mocked.onPost('https://beta.cowsoko.com/api/v1/register').reply(201, resp)
      signupStoreModules.actions.signUpUser({commit}, userDetails).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('should commit signupuser when api call returns a status code of 422', () => {
      const commit = jest.fn()
      mocked.onPost('https://beta.cowsoko.com/api/v1/register').reply(422)
      signupStoreModules.actions.signUpUser({commit}, userDetails).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('should commit when api call returns an error code other than 422', () => {
      const commit = jest.fn()
      mocked.onPost('https://beta.cowsoko.com/api/v1/register').reply(400)
      signupStoreModules.actions.signUpUser({commit}, userDetails).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('should toggle the success property', () => {
      const commit = jest.fn()
      const state = {
        userAuthSignUp: {
          userId: null,
          loading: false,
          success: true,
          status: null
        }}

      signupStoreModules.actions.openValidationPage({commit})
      expect(state.userAuthSignUp.success).toEqual(true)
    })

    // Getter
    it('test for the signedUpUser', () => {
      const state = {
        userAuthSignUp: {
          userId: null,
          loading: false,
          success: true,
          status: null
        }}
      signupStoreModules.getters.signedUpUser(state)
      expect(state).toEqual(state)
    })
  })
  //   MUTATIONS
  it('toggles loader to true or false', () => {
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }}
    // apply mutation
    signupStoreModules.mutations.LOADER(state, true)
    // assert result
    expect(state.userAuthSignUp.loading).toEqual(true)
  })

  it('changes state base on the payload', () => {
    // mock state
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }}
    const payload = {
      user: {
        id: 5
      }
    }

    signupStoreModules.mutations.SIGNED_UP_USER(state, payload)
    expect(state.userAuthSignUp.userId).toEqual(5)
    expect(state.userAuthSignUp.success).toEqual(true)
  })

  it('changes state base on the payload', () => {
    // mock state
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: true,
        status: null
      }}

    signupStoreModules.mutations.TOGGLE_SUCCESS_PROPERTY(state, true)
    expect(state.userAuthSignUp.success).toEqual(true)
  })

  it('Success message should be same as what is passed in the TOGGLE_SUCCESS_PROPERTY params', () => {
    // mock state
    const state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: false,
        status: null,
        errorMessage: ''
      }}

    signupStoreModules.mutations.TOGGLE_SUCCESS_PROPERTY(state, true)
    expect(state.userAuthSignUp.success).toEqual(true)
  })
})
