import { shallowMount, createLocalVue } from '@vue/test-utils'
import ProductsPage from '@/pages/ProductsPage/Products'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

const actions = {
  fetchProducts: jest.fn(),
  fetchCounties: jest.fn()
}

const getters = {
  showPrevious: () => true,
  showNext: () => true,
  USER_PROFILE: () => ({
    county: 'joshua',
    user: {
      county: 'joshua'
    }
  })
}

const state = {
  productsModule: {
    products: [{
      name: 'joshua',
      description: 'joshua',
      price: '200',
      images: 'https://joshua.jpg',
      county: false,
      product_type: 'joshua',
      id: 234
    }],
    isLoading: false,
    counties: ['joshua', 'fredrick']
  }
}

const store = new Vuex.Store({
  state,
  getters,
  actions
})

const $route = {
  query: {
    page: 345
  }
}

const $router = {
  push: jest.fn()
}

const wrapper = shallowMount(ProductsPage, {localVue,
  store,
  mocks: {
    $route,
    $router
  }})

wrapper.vm.$router.push = jest.fn()

const methodSpy = (method) => jest.spyOn(wrapper.vm, method)

describe('Products Page Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('county render when county is null', () => {
    wrapper.vm.$route.query.page = null
    wrapper.vm.$options.watch.county.call(wrapper.vm, 'joshua')
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('watch $route should call this.fetchProducts', () => {
    const to = {
      query: {
        page: 23
      }
    }
    wrapper.vm.$options.watch.$route.call(wrapper.vm, to)
    expect(actions.fetchProducts).toHaveBeenCalled()
  })

  it('watch $route should call this.fetchProducts when page is undefined', () => {
    const to = {
      query: {
        page: null
      }
    }
    wrapper.vm.$options.watch.$route.call(wrapper.vm, to)
    expect(actions.fetchProducts).toHaveBeenCalled()
  })

  it('filterProduct should set selectedCounty to value.county if value.county', () => {
    const filterProductSpy = methodSpy('filterProduct')
    filterProductSpy({
      county: 'fredrick',
      type: 'joshua'
    })
    expect(wrapper.vm.selectedCounty).toEqual('fredrick')
    expect(wrapper.vm.inputType).toEqual('joshua')
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
  })

  it('filterProduct should set selectedCounty to value.county if not value.county', () => {
    const filterProductSpy = methodSpy('filterProduct')
    filterProductSpy({
      type: null
    })
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
  })

  it('goToPreviousPage should call this.router.push', () => {
    const previousSpy = methodSpy('goToPreviousPage')
    previousSpy()
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
  })

  it('goToPreviousPage should call this.router.push', () => {
    getters.USER_PROFILE().county = false
    const newGetters = {...getters,
      USER_PROFILE: () => ({
        county: null,
        user: {
          county: null
        }
      })}
    const newStore = new Vuex.Store({
      actions,
      state,
      getters: newGetters
    })

    const newWrapper = shallowMount(ProductsPage, {localVue,
      store: newStore,
      mocks: {
        $route,
        $router
      }})
    expect(newWrapper.isVueInstance()).toBeTruthy()
  })

  it('goToNextPage should call this.$router.push', () => {
    const nextSpy = methodSpy('goToNextPage')
    nextSpy()
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
  })

  it('goToPreviousPage should call this.router.push when this.$route.query is not defined', () => {
    wrapper.vm.$route.query = null
    const previousSpy = methodSpy('goToPreviousPage')
    previousSpy()
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
  })
})
