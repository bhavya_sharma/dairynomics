import {createLocalVue, shallowMount} from '@vue/test-utils'
import ImagePreview from '@/components/ui/ImagePreview'

const localVue = createLocalVue()

const propsData = { images: [{
  file: {
    name: 'joshua',
    size: 234554,
    imgPreviewUrl: 'https://joshua.jpg'
  }
}]
}

const newPropsData = { images: [{
  file: {
    name: 'joshua',
    size: 9889999999,
    imgPreviewUrl: 'https://joshua.jpg'
  }
}]
}
const wrapper = shallowMount(ImagePreview, {localVue, propsData})

describe('Image Preview test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render when image size is very large', () => {
    const newWrapper = shallowMount(ImagePreview, {propsData: newPropsData, localVue})

    expect(newWrapper.isVueInstance()).toBeTruthy()
  })
})
