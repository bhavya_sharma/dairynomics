import { shallowMount } from '@vue/test-utils'
import UserProfileSwitcher from '@/pages/UserProfileTimeline/Switcher.vue'

describe('UserProfileTimelinePage', () => {
  let data
  beforeEach(() => {
    data = {
      first_name: '',
      last_name: '',
      county: '',
      old_password: '',
      password: '',
      confirm_password: '',
      account_type: 'I am a dairy farmer',
      imageData: null
    }
  })
  const wrapper = shallowMount(UserProfileSwitcher, {
    data})

  it('changes activeStatusLink when switcher is clicked', () => {
    wrapper.find('.my-activities').trigger('click')
    expect(wrapper.vm.isMyActivitiesLinkActive).toEqual(true)
    wrapper.find('.report').trigger('click')
    expect(wrapper.vm.isMyActivitiesLinkActive).toEqual(false)
    expect(wrapper.vm.isReportLinkActive).toEqual(true)
    expect(wrapper.vm.isMyProfileLinkActive).toEqual(false)
    wrapper.find('.my-profile').trigger('click')
    expect(wrapper.vm.isMyActivitiesLinkActive).toEqual(false)
    expect(wrapper.vm.isReportLinkActive).toEqual(false)
    expect(wrapper.vm.isMyProfileLinkActive).toEqual(true)
  })
  it('emits changeProfileSection when any of the switchers are clicked', () => {
    wrapper.find('.my-activities').trigger('click')
    expect(wrapper.emitted('changeProfileSection').length > 0).toBe(true)
    wrapper.find('.report').trigger('click')
    expect(wrapper.emitted('changeProfileSection').length > 0).toBe(true)
    wrapper.find('.my-profile').trigger('click')
    expect(wrapper.emitted('changeProfileSection').length > 0).toBe(true)
  })
})
