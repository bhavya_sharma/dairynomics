import AxiosCalls from '@/utils/api/AxiosCalls'

export default {
  state: {
    farmDetails: {},
    isLoading: true
  },
  mutations: {
    farmDetails: (state, farmData) => {
      state.farmDetails = farmData
      return state.farmDetails
    },
    loading: (state, payload) => {
      state.isLoading = payload
      return state.isLoading
    }
  },
  actions: {
    getFarmDetails ({ commit }, { farmId, router }) {
      commit('loading', true)
      return AxiosCalls.get(
        `api/v1/farms/${farmId}`)
        .then(response => {
          commit('farmDetails', response.data)
          commit('loading', false)
        })
        .catch(error => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default :
              return router.push('/')
          }
        })
    }
  },
  getters: {
    farmInfo (state) {
      return {...state}
    }
  }
}
