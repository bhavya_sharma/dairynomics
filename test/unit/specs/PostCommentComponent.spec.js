import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuelidate from 'vuelidate'
import PostComments from '@/components/comment/PostComments.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuelidate)

const $router = {
  push: jest.fn()
}

const blogData = {
  blog: {},
  blogId: '',
  blogsPerPage: 10,
  blogIds: [13, 15, 20, 30, 43, 45, 47, 50, 51, 52, 53],
  comments: [{
    'id': '1',
    'comment': 'wooow!',
    'created_by': {
      'id': '814',
      'first_name': 'john',
      'last_name': 'thuo',
      'profile': {
        'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg'
      }
    },
    'likes': 10,
    'number_of_replies': 0,
    'created_at': {
      'date': '2019-03-27 19:44:37.000000',
      'timezone_type': 3,
      'timezone': 'Africa/Nairobi'
    },
    'updated_at': {
      'date': '2019-03-27 19:44:37.000000',
      'timezone_type': 3,
      'timezone': 'Africa/Nairobi'
    },
    'replies': [
      {
        'id': '48',
        'reply': 'wooow!',
        'created_by': {
          'id': '814',
          'first_name': 'john',
          'last_name': 'thuo',
          'profile': {
            'pic': ''
          }
        }
      }
    ]

  }],
  comment_reply_id: '',
  reply_id: '',
  comment_relpy: '',
  error: '',
  comment_id: '',
  active_like: true,
  userIds: {},
  likeBlogUserIds: ['814']
}

Storage.prototype.getItem = jest.fn(() => '12')
const getters = {
  getBlogInfo: () => blogData,
  getBlogs: () => {
    return {
      currentpage: 1
    }
  }
}
const actions = {
  sendVerification: jest.fn(),
  getBlog: jest.fn(),
  getAllBlogs: jest.fn(),
  getComments: jest.fn(),
  postComment: jest.fn(),
  editReply: jest.fn(),
  likeSingleComment: jest.fn(),
  removeComment: jest.fn(),
  postReply: jest.fn(),
  deleteComment: jest.fn(),
  setBlogId: jest.fn(),
  removeReply: jest.fn(),
  updateCommentRelpyId: jest.fn(),
  unLikeSingleComment: jest.fn(),
  likeSingleBlog: jest.fn(),
  unLikeSingleBlog: jest.fn(),
  likeSingleReply: jest.fn(),
  unlikeSingleReply: jest.fn(),
  editCommentAction: jest.fn(),
  getReplyLikes: jest.fn(),
  getSingleBlog: jest.fn()
}
const mutations = {
  updateReplyId: jest.fn(),
  updateCommentId: jest.fn(),
  updateCommentReplyId: jest.fn(),
  setComments: jest.fn(),
  setBlogId: jest.fn(),
  setBlog: jest.fn()
}
const store = new Vuex.Store({
  actions, getters, mutations
})

const newBlogs = {...blogData, likeBlogUserIds: ['12', '13']}
const newGetters = {
  getBlogInfo: () => newBlogs,
  getBlogs: () => {
    return {
      currentpage: 1
    }
  }
}

const newStore = new Vuex.Store({
  actions, mutations, getters: newGetters
})

const blogId = 13
const wrapper = shallowMount(PostComments, {
  localVue,
  store,
  data: {
    commentBody: ''
  },
  mocks: { $route: { params: { blogId } }, $router }
})

const methodSpy = (method) => jest.spyOn(wrapper.vm, method)

describe('test PostComments Component', () => {
  it('test makeComment method', () => {
    wrapper.vm.makeComment('event', 'This is a comment')
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('test makeComment method with commentBody set in data', () => {
    const makeCommentSpy = methodSpy('makeComment')
    const postCommentSpy = methodSpy('postComment')
    makeCommentSpy('e', 'joshua')
    expect(postCommentSpy).toHaveBeenCalled()
  })

  it('test makeComment method with commentBody set in data', () => {
    const makeCommentSpy = methodSpy('makeComment')
    const postCommentSpy = methodSpy('postComment')
    wrapper.setData({commentBody: 'joshua'})
    makeCommentSpy('e', 'joshua')
    expect(postCommentSpy).toHaveBeenCalled()
  })

  it('test editComment method ', () => {
    const editCommentSpy = methodSpy('editComment')
    const updateCommentSpy = methodSpy('updateCommentId')
    editCommentSpy(23, 'joshua', true)
    expect(updateCommentSpy).toHaveBeenCalled()
    expect(actions.editCommentAction).toHaveBeenCalled()
  })

  it('test editComment method ', () => {
    const editCommentSpy = methodSpy('editComment')
    const updateCommentSpy = methodSpy('updateCommentId')
    updateCommentSpy.mockReset()
    editCommentSpy(23, 'joshua', false)
    expect(updateCommentSpy).not.toHaveBeenCalled()
  })

  it('test nextArticle method ', () => {
    const nextArticleSpy = methodSpy('nextArticle')
    nextArticleSpy()
    expect(actions.getAllBlogs).toHaveBeenCalled()
    expect($router.push).toHaveBeenCalled()
  })

  it('test nextArticle method ', () => {
    const newWrapper = shallowMount(PostComments, {
      localVue,
      store: newStore,
      data: {
        commentBody: ''
      },
      mocks: { $route: { params: { id: 50 } }, $router }
    })

    const nextArticleSpy = jest.spyOn(newWrapper.vm, 'nextArticle')
    nextArticleSpy()
    expect(actions.getAllBlogs).toHaveBeenCalled()
    expect($router.push).toHaveBeenCalled()
  })

  it('deleteComment method ', () => {
    const deleteCommentSpy = methodSpy('deleteComment')
    const deleteReplySpy = methodSpy('deleteReply')
    deleteCommentSpy({id: 34}, {id: 32})
    expect(deleteReplySpy).toHaveBeenCalled()
  })

  it('deleteComment method ', () => {
    const deleteCommentSpy = methodSpy('deleteComment')
    deleteCommentSpy({id: 34})
    expect(actions.removeComment).toHaveBeenCalled()
  })

  it('postCommentReply method ', () => {
    const postCommentReplySpy = methodSpy('postCommentReply')
    postCommentReplySpy(34, 'joshua')
    expect(actions.postReply).toHaveBeenCalled()
  })

  it('postCommentReply method ', () => {
    const postCommentReplySpy = methodSpy('postCommentReply')
    actions.postReply.mockReset()
    postCommentReplySpy(34, '')
    expect(actions.postReply).not.toHaveBeenCalled()
  })

  it('likeReply method ', () => {
    const likeReplySpy = methodSpy('likeReply')
    likeReplySpy(34)
    expect(actions.likeSingleReply).toHaveBeenCalled()
  })

  it('likeBlog method ', () => {
    const likeBlogSpy = methodSpy('likeBlog')
    likeBlogSpy(34)
    expect(actions.likeSingleBlog).toHaveBeenCalled()
  })

  it('unLikeBlog method ', () => {
    const unLikeBlogSpy = methodSpy('unLikeBlog')
    unLikeBlogSpy(34)
    expect(actions.unLikeSingleBlog).toHaveBeenCalled()
  })

  it('isLikedComment method ', () => {
    const isLikedCommentSpy = methodSpy('isLikedComment')
    const result = isLikedCommentSpy({user_likes: ['12', '13']})
    expect(result).toBeTruthy()
  })

  it('isLikedComment method ', () => {
    const isLikedCommentSpy = methodSpy('isLikedComment')
    const result = isLikedCommentSpy({user_likes: null})
    expect(result).toBeFalsy()
  })

  it('handleLikingComment method ', () => {
    const handleLikingCommentSpy = methodSpy('handleLikingComment')
    const unlikeComment = methodSpy('unlikeComment')
    handleLikingCommentSpy({id: 13, user_likes: ['12', '13']})
    expect(unlikeComment).toHaveBeenCalled()
  })

  it('handleLikingComment method ', () => {
    const handleLikingCommentSpy = methodSpy('handleLikingComment')
    handleLikingCommentSpy({id: 13, user_likes: ['53']})
    expect(actions.likeSingleComment).toHaveBeenCalled()
  })

  it('handleLikingReply method ', () => {
    const handleLikingReplySpy = methodSpy('handleLikingReply')
    const unlikeReplySpy = methodSpy('unlikeReply')
    handleLikingReplySpy({id: 13, user_likes: ['12', '13']})
    expect(unlikeReplySpy).toHaveBeenCalled()
  })

  it('handleLikingReply method ', () => {
    const handleLikingReplySpy = methodSpy('handleLikingReply')
    const likeReplySpy = methodSpy('likeReply')
    handleLikingReplySpy({id: 13, user_likes: ['53']})
    expect(likeReplySpy).toHaveBeenCalled()
  })

  it('likeComment method ', () => {
    const likeCommentSpy = methodSpy('likeComment')
    const handleLikingReplySpy = methodSpy('handleLikingReply')
    likeCommentSpy({id: 13, user_likes: ['12', '13']}, {id: 13, user_likes: ['12', '13']})
    expect(handleLikingReplySpy).toHaveBeenCalled()
  })

  it('likeComment method ', () => {
    const likeCommentSpy = methodSpy('likeComment')
    const handleLikingCommentSpy = methodSpy('handleLikingComment')
    likeCommentSpy({id: 13, user_likes: ['12', '13']})
    expect(handleLikingCommentSpy).toHaveBeenCalled()
  })

  it('watch $route', () => {
    wrapper.vm.$options.watch.$route.call(wrapper.vm, {params: {
      id: 45
    }})
    expect(actions.getSingleBlog).toHaveBeenCalled()
  })

  it('GetBlogInfo watch', () => {
    wrapper.vm.$options.watch.getBlogInfo.call(wrapper.vm, {
      likeBlogUserIds: ['12', '13']
    })
    expect(wrapper.vm.onchange).toBeTruthy()
  })

  it('GetBlogInfo watch', () => {
    wrapper.vm.$options.watch.getBlogInfo.call(wrapper.vm, {
      likeBlogUserIds: ['89', '98']
    })
    expect(wrapper.vm.onchange).toBeFalsy()
  })
})
