import {shallowMount, createLocalVue} from '@vue/test-utils'
import Router from 'vue-router'
import AppTab from '@/components/layout/AppTab.vue'

const localVue = createLocalVue()
localVue.use(Router)

describe('AppTab', () => {
  const $route = {
    name: 'someName'
  }
  const $router = {
    push: jest.fn()
  }
  const wrapper = shallowMount(AppTab, {
    propsData: {
      name: 'testTab',
      icon: 'someIcon',
      activeIcon: 'someActiveIcon',
      pageName: 'somePageName'
    },
    mocks: {
      $route,
      $router
    }
  })
  it('renders a div', () => {
    expect(wrapper.contains('div')).toBe(true)
  })
  it('has a methods hook', () => {
    expect(typeof AppTab.methods).toBe('object')
    const router = new Router({push: {name: 'someName'}})
    const wrapper = shallowMount(AppTab, {localVue, router})
    wrapper.find('.tab-item').trigger('click')
  })
})
