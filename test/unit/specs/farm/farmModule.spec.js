import MockAdapter from 'axios-mock-adapter'
import { createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import Farm from '@/store/modules/Farm'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)
export default {
  state: {
    farms: []
  }
}
describe('Farm module', () => {
  let comment = 'hello'
  let state = {
    blog: {},
    blog_id: 13,
    comments: [{ comment }],
    comment_reply_id: '',
    reply_id: '',
    comment_relpy: '',
    error: '',
    comment_id: 12,
    active_like: true,
    userIds: {},
    likeBlogUserIds: []
  }
  const mocked = new MockAdapter(axios)

  it('test the  setfarms mutation', () => {
    const state = { farms: [] }
    const farms = []
    Farm.mutations.setfarms(state, farms)
  })

  it('test getFarms action', async () => {
    let commit = jest.fn()
    mocked.onGet('https://beta.cowsoko.com/api/v1/farms').reply(200, state)
    await Farm.actions.getFarms(commit)
  })
})
