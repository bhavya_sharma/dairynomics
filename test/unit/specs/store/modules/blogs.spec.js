import blogs from '@/store/modules/blogs.js'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

jest.mock('@/router/router')
const commit = jest.fn()
router.push = jest.fn()
jest.mock('@/store')

describe('mutation for blogs module', () => {
  const state = {
    blogs: [],
    randomBlog: {}
  }

  it('calls the SET_BLOGS mutation', () => {
    blogs.mutations.SET_BLOGS(state, [{ key: 'value' }])
    expect(state.blogs).toEqual([{ key: 'value' }])
  })

  it('calls the SET_RANDOM_BLOG mutation', () => {
    blogs.mutations.SET_RANDOM_BLOG(state, {key: 'value'})
    expect(state.randomBlog).toEqual({key: 'value'})
  })

  it('blog getters should return state.blogs', () => {
    const expectedBlogs = blogs.getters.BLOGS(state)
    expect(expectedBlogs).toEqual(state.blogs)
  })

  it('blog getters should return random blog', () => {
    const expectedBlogs = blogs.getters.RANDOMBLOG(state)
    expect(expectedBlogs).toEqual(undefined)
  })
})
describe('Actions for blogs module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('successfully fetches blogs from the API', () => {
    const response = {
      data: {
        blogs: []
      },
      success: true
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user_blogs').reply(200, response)
    blogs.actions.GET_BLOGS({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should redirect to 404 page when status code is 404', () => {
    mock.onGet('https://beta.cowsoko.com/api/v1/user_blogs').reply(404)
    blogs.actions.GET_BLOGS({commit}).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should redirect to 404 page when status code is 404', () => {
    router.push.mockReset()
    mock.onGet('https://beta.cowsoko.com/api/v1/user_blogs').reply(400)
    blogs.actions.GET_BLOGS({commit}).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })
})
