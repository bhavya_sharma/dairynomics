import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import router from '@/router/router'

export default {
  state: {
    experts: [],
    randomExpert: {},
    isLoading: false
  },
  getters: {
    EXPERT_PROFILES (state) {
      return state.experts
    },
    RANDOM_EXPERT (state) {
      return state.randomExpert
    },
    IS_LOADING (state) {
      return state.isLoading
    }
  },
  mutations: {
    SET_EXPERT_PROFILES (state, experts) {
      state.experts = experts
    },
    SET_RANDOM_EXPERT (state, payload) {
      state.randomExpert = payload
    },
    SET_ISLOADING (state, payload) {
      state.isLoading = payload
    }
  },
  actions: {
    GET_EXPERT_PROFILES: ({commit}) => {
      commit('SET_ISLOADING', true)
      return AxiosCalls.get('api/v1/user_experts')
        .then((response) => {
          const randomExpert = getRandomItem(response.data)
          commit('SET_EXPERT_PROFILES', response.data)
          commit('SET_RANDOM_EXPERT', randomExpert)
          commit('SET_ISLOADING', false)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
