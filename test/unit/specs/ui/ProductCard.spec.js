import { shallowMount, createLocalVue } from '@vue/test-utils'
import vueRouter from 'vue-router'
import ProductCard from '@/components/ui/ProductCard.vue'

const localVue = createLocalVue()

localVue.use(vueRouter)

const propsData = {
  productName: 'joshua',
  description: 'joshua',
  price: '$250',
  id: 45,
  images: ['https://joshua1.jpg', 'https://joshua2.jpg'],
  county: 'joshua',
  category: 'joshua',
  view: 'joshua'
}
const wrapper = shallowMount(ProductCard, {propsData: propsData, localVue})

describe('Product Card test', () => {
  it('should render product card', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render when description length is greater than 100', () => {
    wrapper.setProps({
      description: 'joshuajoshuajoshuajoshuajoshuajoshuajoshuajsohuasjoshuajoshuajoshuajoshuajoshuajoshuajoshsuajoshuajoshsuajoshuajoshousjoshuajosjoajoshuajoshuajoshuajosuajoshuajoshuajoshuajoshua'
    })
    expect(wrapper.find('.description').text().length).toEqual(104)
  })
})
