import { shallowMount, createLocalVue } from '@vue/test-utils'
import 'isomorphic-fetch'
import Vuex from 'vuex'
import Vuelidate from 'vuelidate'
import AxiosMockAdapter from 'axios-mock-adapter'
import axios from 'axios'
import UserAuthSignUp from '@/components/ui/userAuth/UserAuthSignUp'
import landingPage from '@/store/modules/landingPage'

const mock = new AxiosMockAdapter(axios)

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(Vuelidate)

describe('UserAuthSignUp', () => {
  let getters
  let store
  let data
  let wrapper
  let actions
  let state
  let watch

  const response = {
    country_code: 'NG',
    country_name: 'Nigeria',
    city: 'Lagos',
    postal: null,
    latitude: 6.4531,
    longitude: 3.3958,
    IPv4: '41.215.245.118',
    state: 'Lagos'
  }

  mock.onGet('https://geoip-db.com/json/').reply(200, response)
  beforeEach(() => {
    // wrapper = shallow(UserAuthSignUp)
    watch = {
      signedUpUser: jest.fn()
    }

    actions = {
      signUpUser: jest.fn(),
      openValidationPage: jest.fn()
    }

    data = {
      signupForm: {
        first_name: '',
        last_name: '',
        phone: '',
        county: '',
        password: '',
        confirm_password: '',
        country_code: '',
        account_type: ''
      }
    }

    state = {
      userAuthSignUp: {
        userId: null,
        loading: false,
        success: null,
        status: null
      }
    }
    getters = {
      signedUpUser: () => state.userAuthSignUp
    }
    store = new Vuex.Store({
      data,
      watch,
      modules: {
        signupStoreModules: {
          actions,
          getters,
          state
        },
        landingPage
      }
    })
    wrapper = shallowMount(UserAuthSignUp, { localVue, store })
  })
  jest.mock('@/store')
  it('should render the component UserAuthSignUp', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should toggle passwordFieldType property', () => {
    wrapper.setData({passwordFieldType: 'password'})
    expect(wrapper.vm.passwordFieldType).toEqual('password')
    wrapper.find('button[id="pass').trigger('click')
    expect(wrapper.vm.passwordFieldType).toEqual('text')
    wrapper.find('button[id="pass').trigger('click')
    expect(wrapper.vm.passwordFieldType).toEqual('password')
  })

  it('should toggle submitSignUpForm and throw an error when form is invalid', () => {
    wrapper.setData({signupForm: {
      first_name: '',
      last_name: '',
      phone: '',
      county: '',
      password: '4',
      confirm_password: '4444',
      country_code: '+256',
      account_type: 'I am a diary farmer'
    }})
    wrapper.find('button[type="button').trigger('click')
  })

  it('should trigger the haveRegistrationPin method ', () => {
    wrapper.find('a').trigger('click')
    expect(actions.openValidationPage).toHaveBeenCalled()
  })

  it('should select user type if supplied by the store and be unable to change', () => {
    landingPage.state.userType = 'I am an input seller'

    expect(wrapper.find('#exampleFormControlSelect1').attributes('disabled')).toBeTruthy()
    expect(wrapper.vm.signupForm.account_type).toEqual('I am a dairy farmer')
  })
})
