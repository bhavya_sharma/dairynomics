import UserTests from '@/store/modules/UserTests'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

router.push = jest.fn()

jest.mock('@/store')
jest.mock('@/router/router')

describe('mutation for user tests module', () => {
  const state = {
    tests: [],
    randomTest: {}
  }
  it('calls the SET_USER_TESTS mutation', () => {
    UserTests.mutations.SET_USER_TESTS(state, [{ key: 'value' }])
    expect(state.tests).toEqual([{ key: 'value' }])
  })
  it('calls the SET_RANDOM_USER_TEST mutation', () => {
    UserTests.mutations.SET_RANDOM_USER_TEST(state, {key: 'value'})
    expect(state.randomTest).toEqual({key: 'value'})
  })
})
describe('Getters for user tests module', () => {
  const state = {
    tests: [],
    randomTest: {}
  }
  it('tests the USER_TESTS getter', () => {
    const tests = UserTests.getters.USER_TESTS(state)
    expect(tests).toEqual(state.tests)
  })
  it('tests the RANDOM_USER_TEST getter', () => {
    const test = UserTests.getters.RANDOM_USER_TEST(state)
    expect(test).toEqual(state.randomTest)
  })
})
describe('Actions for user tests module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('successfully fetches recommended tests from the API', () => {
    const commit = jest.fn()
    const response = []
    mock.onGet('https://beta.cowsoko.com/api/v1/onboarding_test').reply(200, response)
    UserTests.actions.GET_USER_TESTS({commit}).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })
  it('should return a 404 page when an error occurs', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/onboarding_test').reply(404, response)
    UserTests.actions.GET_USER_TESTS({ commit }).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should call router.push when the statuscode is not 404', () => {
    const commit = jest.fn()
    const response = {
      success: false,
      error: 'some error'
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/onboarding_test').reply(400, response)
    UserTests.actions.GET_USER_TESTS({ commit }).then(() => {
      expect(router.push).toHaveBeenCalled()
    })
  })
})
