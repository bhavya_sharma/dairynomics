
import UserProfileModule from '@/store/modules/UserProfileModule'
import { mock } from '@/store/modules/mocks/mockInstance'

let localStorageFn
const commit = jest.fn()
jest.mock('@/store')

describe('mutation for UserProfile module', () => {
  const state = {
    user: {}
  }
  it('calls the SET_USER_PROFILE mutation', () => {
    UserProfileModule.mutations.SET_USER_PROFILE(state, { key: 'value' })
    expect(state.user).toEqual({ key: 'value' })
  })

  it('calls the SET_IS_LOADING mutation', () => {
    UserProfileModule.mutations.SET_IS_LOADING(state, false)
    expect(state.isLoading).toEqual(false)
  })

  it('calls the SET_PROFILE_PIC mutation', () => {
    UserProfileModule.mutations.SET_PROFILE_PIC(state, 'https://joshua.jpg')
    expect(state.profilePic).toEqual('https://joshua.jpg')
  })

  it('calls the SET_USER_PROFILE_ERROR mutation', () => {
    UserProfileModule.mutations.SET_USER_PROFILE_ERROR(state, 'error')
    expect(state.error).toEqual('error')
  })
})
describe('Getters for userProfile module', () => {
  const state = {
    user: {},
    profilePic: 'https://joshua.jpg',
    error: 'error',
    isLoading: false

  }
  it('tests the USER_PROFILE getter', () => {
    const user = UserProfileModule.getters.USER_PROFILE(state)
    expect(user).toEqual(state.user)
  })

  it('tests the PROFILE_PIC getter', () => {
    const profilePic = UserProfileModule.getters.PROFILE_PIC(state)
    expect(profilePic).toEqual(state.profilePic)
  })

  it('tests the USER_PROFILE_LOADING getter', () => {
    const expectedIsLoading = UserProfileModule.getters.USER_PROFILE_LOADING(state)
    expect(expectedIsLoading).toEqual(state.isLoading)
  })

  it('tests the USER_PROFILE_ERROR getter', () => {
    const expectedError = UserProfileModule.getters.USER_PROFILE_ERROR(state)
    expect(expectedError).toEqual(state.error)
  })
})
describe('Actions for UserProfile module', () => {
  afterEach(() => {
    mock.reset()
  })

  it('should return get user profile for a valid token', () => {
    const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQzfQ.E0sqYmlre_Kl5u1e7fEwjGjDlJwzOLc2Y2SfHm_hx_0'
    const context = {
      commit
    }
    const response = {
      data: {
        profile: {
          pic: 'https://joshua.jpg'
        }
      }
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/user/43').reply(200, response)
    localStorageFn = jest.fn(() => token)
    Storage.prototype.getItem = localStorageFn

    UserProfileModule.actions.GET_USER_PROFILE(context).then(() => {
      expect(context.commit).toHaveBeenCalled()
    })
  })

  it('should throw error when token is null', () => {
    localStorageFn.mockClear()
    const context = {
      commit
    }
    const response = {
      data: {
        id: 2174
      }
    }

    mock.onGet('https://beta.cowsoko.com/api/v1/user/null').reply(200, response)
    UserProfileModule.actions.GET_USER_PROFILE(context).then(() => {
      expect(localStorage.getItem).toHaveBeenCalled()
    })
      .catch((error) => {
        expect(error).toBeTruthy()
      })
  })
})
