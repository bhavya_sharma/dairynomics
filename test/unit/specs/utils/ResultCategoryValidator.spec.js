import { resultCategory } from '@/utils/validators/ResultCategoryValidator'

describe('Additional Test for Result Category Validator', () => {
  it('averageCategorycheck range should return nothing when value is not in the range of 11 and 19', () => {
    const expectedResult = resultCategory.averageCategoryCheck(9)
    expect(expectedResult).toBeUndefined()
  })
})
