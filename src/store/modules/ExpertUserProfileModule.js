import AxiosCalls from '@/utils/api/AxiosCalls'
export default {
  state: {
    getExpertProfile: {},
    ExpertProfileError: null,
    setExpertFarms: {},
    expertFarmError: null,
    setExpertBlogs: {},
    expertBlogError: null,
    expertLoading: true
  },
  mutations: {
    GetExpertProfile: (state, user) => (
      state.getExpertProfile = user),
    ProfileErrors: (state, errorMessage) => {
      state.ExpertProfileError = errorMessage
    },
    SET_EXPERT_FARMS: (state, farm) => {
      state.setExpertFarms = farm
    },
    SET_FARMS_ERROR: (state, error) => {
      state.expertFarmError = error
    },
    SET_EXPERT_BLOGS: (state, blog) => {
      state.setExpertBlogs = blog
    },
    SET_BLOGS_ERROR: (state, error) => {
      state.expertBlogError = error
    },
    TOGGLE_LOADING: (state, payload) => {
      state.expertLoading = payload
    }
  },
  actions: {
    expertProfile ({
      commit
    }, id) {
      commit('TOGGLE_LOADING', true)
      return AxiosCalls.get(`api/v1/experts/${id}`)
        .then(response => {
          commit('GetExpertProfile', response.data.data)
          commit('TOGGLE_LOADING', false)
        })
        .catch(() => {
          commit('TOGGLE_LOADING', false)
          commit('ProfileErrors', 'Unable to get the expert profile information, Please try again ')
          commit('ProfileErrors', '')
        })
    },
    expertFarms ({
      commit
    }, id) {
      commit('SET_FARMS_ERROR', false)
      commit('SET_EXPERT_FARMS', false)
      return AxiosCalls.get(`api/v1/experts/${id}/farms`)
        .then((response) => {
          commit('SET_EXPERT_FARMS', response.data.data)
        })
        .catch(() => {
          commit('SET_FARMS_ERROR', 'No content found')
        })
    },
    expertBlogs ({
      commit
    }, id) {
      commit('SET_BLOGS_ERROR', false)
      commit('SET_EXPERT_BLOGS', false)
      return AxiosCalls.get(`api/v1/experts/${id}/blogs`)
        .then((response) => {
          commit('SET_EXPERT_BLOGS', response.data.data.blogs)
        })
        .catch(() => {
          commit('SET_BLOGS_ERROR', 'No content found')
        })
    }
  },

  getters: {
    getExpertProfileInformation (state) {
      return state
    },
    getExpertFarms (state) {
      return state.setExpertFarms
    },
    getExpertFarmsError (state) {
      return state.expertFarmError
    },
    getExpertBlogs (state) {
      return state.setExpertBlogs
    },
    getExpertBlogsError (state) {
      return state.expertBlogError
    },
    getExpertLoadingState (state) {
      return state.expertLoading
    }
  }
}
