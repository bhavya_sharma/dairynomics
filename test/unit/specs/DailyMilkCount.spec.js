import { shallowMount } from '@vue/test-utils'
import DailyMilkCount from '@/pages/DailyMilkPage/DailyMilkCount.vue'
import IntegerInput from '@/components/ui/IntegerInput.vue'

describe('@/pages/DailyMilkPage/DailyMilkCount.vue', () => {
  const wrapper = shallowMount(DailyMilkCount)
  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<h5>How many litres of milk does your cow produce per day?</h5>')
  })
  it('should render an error markup if quantity of milk is negative', () => {
    wrapper.setData({ quantityOfMilk: -2 })
    expect(wrapper.html()).toContain('<div class="error-message"><p>Quantity cannot be negative</p></div>')
  })
  it('should render an error markup if quantity of milk is zero', () => {
    wrapper.setData({ quantityOfMilk: 0, disabled: true })
    expect(wrapper.html()).toContain('<div class="error-message"><p>Quantity cannot be zero</p></div>')
  })
  it('should render an error markup if quantity of milk is empty', () => {
    wrapper.setData({ quantityOfMilk: '', disabled: true })
    expect(wrapper.html()).toContain('<div class="error-message"><p>Quantity of milk is required</p></div>')
  })
  it('should render an error markup if quantity of milk is greater than the maximum of 50 litres', () => {
    wrapper.setData({ quantityOfMilk: 51, disabled: true })
    expect(wrapper.html()).toContain('<div class="error-message"><p>Quantity cannot be greater than 50 litres</p></div>')
  })
  it(`should trigger handleSubmit method if there's no error in input value
  and the keyup.enter event is trigger`, () => {
    wrapper.setData({ quantityOfMilk: 20 })
    wrapper.find(IntegerInput).vm.$emit('enter')
    expect(wrapper.emitted('quantityOfMilk').length).toEqual(1)
  })
})
