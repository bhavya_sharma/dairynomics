import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualProductsDetails from '@/pages/IndividualProduct/IndividualProductsDetails'

const localVue = createLocalVue()
localVue.use(Vuex)
let store

describe('individualProductDetails', () => {
  let getters
  beforeEach(() => {
    getters = {
      getIndividualProduct: () => {
        return {
          IndividualProductDetails: {}
        }
      }
    }
    store = new Vuex.Store({getters
    })
  })

  it('should render the individualProductDetails component', () => {
    let wrapper = shallowMount(IndividualProductsDetails, {store, localVue})
    expect(wrapper.vm).toBeTruthy()
  })
})
