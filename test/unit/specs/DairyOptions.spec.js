import { mount } from '@vue/test-utils'
import DairyOptions from '@/pages/LandingPage/DairyOptions.vue'

describe('@pages/LandingPage/DairyOptions.vue', () => {
  const propsObject = {
    propsData: { description: 'I am an input seller or service provider' }
  }
  const wrapper = mount(DairyOptions, propsObject)

  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<i class="fa fa-check dairy-option-check"></i>')
  })
  it('emits handleDairyOptionClick on clicking the dairy option', () => {
    wrapper.find('.dairy-option').trigger('click')
    expect(wrapper.emitted('handleDairyOptionClick').length).toEqual(1)
  })
})
