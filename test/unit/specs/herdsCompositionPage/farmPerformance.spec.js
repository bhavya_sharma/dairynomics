import { shallowMount } from '@vue/test-utils'
import FarmPerformance from '@/pages/HerdsCompositionPage/FarmPerformance'

jest.mock('@/mixins/isLoggedInMixin')

describe('FarmPerformance', () => {
  it('should render correct message when performace is 80% and above', () => {
    const wrapper = shallowMount(FarmPerformance, {
      propsData: {
        numberOfCows: 10,
        numberOfMilked: 8
      }
    })

    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.find('h2').text()).toEqual('Congratulations!')
    expect(wrapper.find('.info-message').text())
      .toEqual('You\'re doing pretty well! But you can always do better.')
  })

  it('should render correct message when performace is 70% and above', () => {
    const wrapper = shallowMount(FarmPerformance, {
      propsData: {
        numberOfCows: 10,
        numberOfMilked: 7
      }
    })

    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.find('h2').text())
      .toEqual('Er.. Ok')
    expect(wrapper.find('.info-message').text())
      .toEqual('You\'re doing Ok, but you can do way better!')
  })

  it('should render correct message when performace is 50% and above', () => {
    const wrapper = shallowMount(FarmPerformance, {
      propsData: {
        numberOfCows: 10,
        numberOfMilked: 5
      }
    })

    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.find('h2').text())
      .toEqual('Below Average')
    expect(wrapper.find('.info-message').text())
      .toEqual('Your farm can perform so much better!')
  })
})
