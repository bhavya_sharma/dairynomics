import LivestockModule from '@/store/modules/Livestock'
import { mock } from '@/store/modules/mocks/mockInstance'
import router from '@/router/router'

router.push = jest.fn()

jest.mock('@/router/router')
jest.mock('@/store')

const state = {
  livestock: 'joshua_livestock',
  metaDetails: 'joshua_metaDetails',
  livestockTypeSelected: 'joshua',
  livestockBreedSelected: 'joshua'
}

const options = {
  filteredBy: {
    name: 'joshua'
  },
  page: 34
}

afterEach(() => {
  mock.reset()
})

describe('Livestock Module Test', () => {
  describe('Livestock mutations test', () => {
    const payload = {
      livestock_type: 'joshua',
      data: {
        livestock: 'joshua_livestock'
      },
      breeds: 'joshua_breeds'
    }
    const { mutations: { SET_LIVESTOCK, SET_LIVESTOCK_META_DETAILS, SET_LIVESTOCK_TYPES, SET_LIVESTOCK_BREEDS, TOGGLE_IS_LOADING, SET_LIVESTOCK_META_DETAILS_ERROR, SET_LIVESTOCK_BREEDS_ERROR, SET_LIVESTOCK_ERROR } } = LivestockModule

    it('SET_LIVESTOCK should set livestock to data.livestock', () => {
      SET_LIVESTOCK(state, payload)
      expect(state.livestock).toEqual('joshua_livestock')
    })

    it('SET_LIVESTOCK_META_DETAILS_ERROR', () => {
      SET_LIVESTOCK_META_DETAILS_ERROR(state)
      expect(state.metaDetails).toEqual({})
    })

    it('SET_LIVESTOCK_BREEDS_ERROR', () => {
      SET_LIVESTOCK_BREEDS_ERROR(state)
      expect(state.livestockBreeds).toEqual([])
    })

    it('SET_LIVESTOCK_META_DETAILS should set metaDetails to payload', () => {
      SET_LIVESTOCK_META_DETAILS(state, payload)
      expect(state.metaDetails).toEqual(payload)
    })

    it('SET_LIVESTOCK_ERROR', () => {
      SET_LIVESTOCK_ERROR(state)
      expect(state.livestock).toEqual([])
    })

    it('SET_LIVESTOCK_TYPES should set livestockTypes to breeds', () => {
      SET_LIVESTOCK_TYPES(state, payload)
      expect(state.livestockTypes).toBeTruthy()
    })

    it('SET_LIVESTOCK_BREEDS should set livestockbreeds to breeds', () => {
      SET_LIVESTOCK_BREEDS(state, payload)
      expect(state.livestockBreeds).toBeTruthy()
    })

    it('TOGGLE_IS_LOADING should set isLoading with payload', () => {
      TOGGLE_IS_LOADING(state, true)
      expect(state.isLoading).toBeTruthy()
    })
  })

  describe('Livestock action test', () => {
    const context = {
      commit: jest.fn()
    }
    const { actions: { FETCH_LIVESTOCK, FETCH_LIVESTOCK_TYPES, FETCH_LIVESTOCK_BREEDS, FILTER_LIVESTOCK } } = LivestockModule
    Storage.prototype.getItem = jest.fn(() => 'joshua')

    it('fetch_livestock action test should call context.commit when is not provided', () => {
      const response = {
        meta: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/livestocks?page=1').reply(200, response)
      FETCH_LIVESTOCK(context).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('fetch_livestock action test should call context.commit', () => {
      const response = {
        meta: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/livestocks?page=32').reply(200, response)
      FETCH_LIVESTOCK(context, 32).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('fetch_livestock should call router.push when statuscode is 404', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestocks?page=89').reply(404, 'error')
      FETCH_LIVESTOCK(context, 89).then({}).catch(() => {
        expect(router.push).toHaveBeenCalledWith('/404')
      })
    })

    it('fetch_livestock should call router.push when statuscode is 400', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestocks?page=89').reply(400)
      FETCH_LIVESTOCK(context, 89).then(() => {
        expect(router.push).toHaveBeenCalledWith('/')
      })
    })

    it('fetch_livestock_types action test should call context.commit', () => {
      const response = {
        meta: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock_type').reply(200, response)
      FETCH_LIVESTOCK_TYPES(context, 32).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('fetch_livestock_type should call router.push when statuscode is 404', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock_type').reply(404)
      FETCH_LIVESTOCK_TYPES(context, 89).then(() => {
        expect(router.push).toHaveBeenCalledWith('/404')
      })
    })

    it('fetch_livestock_type should call router.push when statuscode is 400', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock_type').reply(400)
      FETCH_LIVESTOCK_TYPES(context, 89).then(() => {
        expect(router.push).toHaveBeenCalledWith('/')
      })
    })

    it('fetch_livestock_breeds action test should call context.commit', () => {
      const response = {
        meta: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock_breed/32').reply(200, response)
      FETCH_LIVESTOCK_BREEDS(context, 32).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('fetch_livestock_breeds should call router.push when statuscode is 404', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock_breed/89').reply(404)
      FETCH_LIVESTOCK_BREEDS(context, 89).then(() => {
        expect(router.push).toHaveBeenCalledWith('/404')
      })
    })

    it('fetch_livestock_breeds should call router.push when statuscode is 400', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock_breed/89').reply(400)
      FETCH_LIVESTOCK_BREEDS(context, 89).then(() => {
        expect(router.push).toHaveBeenCalledWith('/')
      })
    })

    it('filter_livestock action test should call context.commit', () => {
      const response = {
        meta: 'joshua'
      }

      mock.onGet('https://beta.cowsoko.com/api/v1/livestock?filter[name]=joshua&page[number]=34').reply(200, response)
      FILTER_LIVESTOCK(context, options).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('filter_livestock action test should call context.commit', () => {
      const response = {
        meta: 'joshua'
      }

      const option = {
        filteredBy: {

        },
        page: 24
      }

      mock.onGet('https://beta.cowsoko.com/api/v1/livestock?page[number]=24').reply(200, response)
      FILTER_LIVESTOCK(context, option).then(() => {
        expect(context.commit).toHaveBeenCalled()
      })
    })

    it('filter_livestock should call router.push when statuscode is 404', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock?filter[name]=joshua&page[number]=34').reply(404)
      FILTER_LIVESTOCK(context, options).then(() => {
        expect(router.push).toHaveBeenCalledWith('/404')
      })
    })

    it('filter_livestock should call router.push when statuscode is 400', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/livestock?filter[name]=joshua&page[number]=34').reply(400)
      FILTER_LIVESTOCK(context, options).then(() => {
        expect(router.push).toHaveBeenCalledWith('/')
      })
    })
  })
})
