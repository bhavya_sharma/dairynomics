import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuelidate from 'vuelidate'
import Farms from '@/pages/Farm/Farms.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Vuelidate)
let store, getters
describe('farm component', () => {
  getters = {
    getState: () => {
      return {
        farms: []
      }
    }
  }
  store = new Vuex.Store({
    getters
  })
  it('this is tests out the farms component', () => {
    // eslint-disable-next-line no-unused-vars
    const wrapper = shallowMount(Farms, { localVue, store })
  })
})
