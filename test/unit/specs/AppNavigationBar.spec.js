import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import AppNavigationBar from '@/components/layout/AppNavigationBar.vue'
import DropDown from '@/components/ui/DropDown.vue'

// jest.mock('@/store')

let localVue = createLocalVue()
localVue.use(Vuex)

describe('AppNavigationBar', () => {
  let actions
  let getters
  let store
  let wrapper

  describe('not signed in user', () => {
    beforeAll(() => {
      const state = {
        isLoggedIn: false
      }
      actions = {
        fetchNotifications: jest.fn(),
        markAsRead: jest.fn(),
        GET_USER_PROFILE: jest.fn()
      }
      getters = {
        PROFILE_PIC: () => '',
        isLoggedIn (state) {
          return { status: state.isLoggedIn }
        },
        getNotifications: () => ({ notifications: [] })
      }
      store = new Vuex.Store({ state, actions, getters })
      wrapper = shallowMount(AppNavigationBar, { store, localVue, stubs: ['router-link'] })
    })

    it('renders correctly', () => {
      expect(wrapper.isVueInstance()).toBeTruthy()
      expect(wrapper.vm.userDropdownMenu).toHaveLength(2)
    })

    it('should not call "fetchNotification"', () => {
      expect(actions.fetchNotifications).not.toHaveBeenCalled()
    })

    it('should not show notification and message icons', () => {
      expect(wrapper.contains(DropDown)).toBe(false)
      expect(wrapper.findAll('.nav-item')).toHaveLength(1)
    })

    it('should call "fetchNotification" when user logs in', () => {
      store.state.isLoggedIn = true
      expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
    })
  })

  describe('signed in user', () => {
    describe('user without new notification', () => {
      beforeAll(() => {
        actions = {
          fetchNotifications: jest.fn(),
          markAsRead: jest.fn(),
          GET_USER_PROFILE: jest.fn()
        }
        getters = {
          PROFILE_PIC: () => '',
          isLoggedIn: () => ({ status: true }),
          getNotifications: () => ({
            notifications: [
              { viewed: '1' }
            ]
          })
        }
        store = new Vuex.Store({ actions, getters })
        wrapper = shallowMount(AppNavigationBar, { store, localVue, stubs: ['router-link'] })
      })

      it('renders correctly', () => {
        expect(wrapper.isVueInstance()).toBeTruthy()
        expect(wrapper.vm.userDropdownMenu).toHaveLength(3)
      })

      it('should call "fetchNotification"', () => {
        expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
      })

      it('should show notification and message icons', () => {
        expect(wrapper.contains(DropDown)).toBe(false)
        expect(wrapper.findAll('.nav-item')).toHaveLength(3)
      })

      it('should not show new notification icon', () => {
        expect(wrapper.findAll('.new-notification')).toHaveLength(0)
      })

      it('should show "DropDown" on clicking "bell icon"', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(true)
      })

      it('should toggle "DropDown" to not show on clicking "bell icon" again', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(false)
      })
    })

    describe('user with new notification', () => {
      beforeAll(() => {
        actions = {
          fetchNotifications: jest.fn(),
          markAsRead: jest.fn(),
          GET_USER_PROFILE: jest.fn()
        }
        getters = {
          PROFILE_PIC: () => '',
          isLoggedIn: () => ({ status: true }),
          getNotifications: () => ({
            notifications: [
              { viewed: '1' },
              { viewed: '0' }
            ]
          })
        }
        store = new Vuex.Store({ actions, getters })
        wrapper = shallowMount(AppNavigationBar, { store, localVue, stubs: ['router-link'] })
      })

      it('renders correctly', () => {
        expect(wrapper.isVueInstance()).toBeTruthy()
        expect(wrapper.vm.userDropdownMenu).toHaveLength(3)
      })

      it('should call "fetchNotification"', () => {
        expect(actions.fetchNotifications).toHaveBeenCalledTimes(1)
      })

      it('should show notification and message icons', () => {
        expect(wrapper.contains(DropDown)).toBe(false)
        expect(wrapper.findAll('.nav-item')).toHaveLength(3)
      })

      it('should show new notification icon', () => {
        expect(wrapper.findAll('.new-notification')).toHaveLength(1)
      })

      it('should show "DropDown" and call "markAsRead on clicking "bell icon"', () => {
        wrapper.find('.bell-nav-icon-link').trigger('click')
        expect(wrapper.contains(DropDown)).toBe(true)
        expect(actions.markAsRead).toHaveBeenCalledTimes(1)
      })
    })
  })
})
