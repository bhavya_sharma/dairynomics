import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuelidate from 'vuelidate'
import datePicker from '@/pages/ServiceProviderOnBoarding/DatePicker'

const localVue = createLocalVue()

localVue.use(Vuelidate)

describe('datePicker', () => {
  let wrapper

  it('should render datePicker component and call model method', () => {
    wrapper = shallowMount(datePicker, { localVue })
    wrapper.setData({ model: new Date() })
    wrapper.setData({ model: new Date() })
  })
})
