import { shallowMount, createLocalVue } from '@vue/test-utils'
import InputType from '@/pages/InputSellerActivity/InputType'
const localVue = createLocalVue()
const propsData = {
  InputHeader: 'Cellfam',
  InputOptions: [1, 2, 3, 4, 5]
}

const wrapper = shallowMount(InputType, {
  localVue,
  propsData,
  attrs: { value: 'sam' }
})

describe('Input Type Component', () => {
  it('should render the Input Type component', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should show that showInputs Method is called correctly', () => {
    const showInputSpy = jest.spyOn(wrapper.vm, 'showInputs')
    showInputSpy()
    expect(wrapper.vm.showSection).toBeTruthy()
    expect(wrapper.vm.toggleSign).toBeFalsy()
  })

  it('should show that closeInputs Method is called correctly', () => {
    const closeInputSpy = jest.spyOn(wrapper.vm, 'closeInputs')
    closeInputSpy()
    expect(wrapper.vm.showSection).toBeFalsy()
    expect(wrapper.vm.toggleSign).toBeTruthy()
  })

  it('should call watch method', () => {
    wrapper.vm.$options.watch.values.call(wrapper.vm, 'cellfam')
    expect(wrapper.emitted().input).toBeTruthy()
  })
})
