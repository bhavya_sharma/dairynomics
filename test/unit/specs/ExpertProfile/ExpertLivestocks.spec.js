import {shallowMount, createLocalVue} from '@vue/test-utils'
import Vuex from 'vuex'
import ExpertLivestocksPage from '@/pages/ExpertProfilePage/ExpertLivestocks.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
const getters = {
  LIVESTOCK_LIST: () => ([
    'joshua', 'fredrick'
  ]),
  LIVESTOCK_META_DETAILS: () => ({
    name: 'joshua'
  }),
  getExpertFarmsError: () => false
}

const state = {
  expertList: {
    counties: ['joshua', 'fredrick']
  },
  Livestock: {
    livestockTypes: ['joshua', 'fredrick'],
    livestockBreeds: ['joshua', 'fredrick'],
    isLoading: true,
    livestock: ['joshua', 'fredrick'],
    metaDetails: {name: 'joshua'}
  }
}

const $route = {
  params: {
    id: 345
  }
}

const mockAction = jest.fn()

const actions = {
  FETCH_EXPERT_LIVESTOCK: mockAction,
  FETCH_LIVESTOCK_TYPES: mockAction,
  FETCH_LIVESTOCK_BREEDS: mockAction
}

const store = new Vuex.Store({
  state,
  actions,
  getters
})

const wrapper = shallowMount(ExpertLivestocksPage, {localVue,
  store,
  mocks: {
    $route
  }})

const methodSpy = (method) => jest.spyOn(wrapper.vm, method)

describe('ExpertiveStock Page Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('livestockTypesOptionsTransformed should return an array', () => {
    const changeProfileSpy = methodSpy('changeProfileSection')
    changeProfileSpy('joshua')
    expect(wrapper.vm.activeStatusLink).toEqual('isMyFarmLinkActive')
  })

  it('livestockBreedsOptionsTransformed should get value', () => {
    const expectedResult = wrapper.vm.livestockBreedsOptionsTransformed
    expect(expectedResult.length).toBeTruthy()
  })

  it('livestockTypesOptionsTransformed', () => {
    const expectedResult = wrapper.vm.livestockTypesOptionsTransformed
    expect(expectedResult.length).toBeTruthy()
  })

  it('countyOptionsTranformed', () => {
    const expectedResult = wrapper.vm.countyOptionsTransformed
    expect(expectedResult.length).toBeTruthy()
  })

  it('showLoading', () => {
    const expectedResult = wrapper.vm.showLoading
    expect(expectedResult).toBeTruthy()
  })

  it('showLoading should return true when this.getExpertFarmsError is true', () => {
    const newGetters = {...getters, getExpertFarmsError: () => true}
    const newStore = new Vuex.Store({
      state,
      actions,
      getters: newGetters
    })
    const newWrapper = shallowMount(ExpertLivestocksPage, {localVue,
      store: newStore,
      mocks: {
        $route
      }})
    expect(newWrapper.vm.showLoading).toBeTruthy()
  })
})
