import axios from 'axios'
import ExpertUserProfileModule from '@/store/modules/ExpertUserProfileModule'
import AxiosMockAdapter from 'axios-mock-adapter'

jest.mock('@/store')

describe('GetExpertProfile mutation', () => {
  it('View expert profile details', () => {
    const state = {}
    let data = ['firstName', 'secondName', 'county', 'expert_category', 'pic', 'course_studied', 'institution', 'training_level', 'start_date', 'finish_date', 'bio']

    ExpertUserProfileModule.mutations.GetExpertProfile(state, data)
    expect(state).toEqual({getExpertProfile: ['firstName', 'secondName', 'county', 'expert_category', 'pic', 'course_studied', 'institution', 'training_level', 'start_date', 'finish_date', 'bio']})
    ExpertUserProfileModule.mutations.ProfileErrors(state)
    expect(state).toEqual({ExpertProfileError: undefined, 'getExpertProfile': ['firstName', 'secondName', 'county', 'expert_category', 'pic', 'course_studied', 'institution', 'training_level', 'start_date', 'finish_date', 'bio']})
  })
  it('commits GetExpertProfile mutation when the page loads', (done) => {
    let mock = new AxiosMockAdapter(axios)
    const commit = jest.fn()

    mock.onGet(`https://beta.cowsoko.com/api/v1/experts/841`).reply(200, {
    })

    ExpertUserProfileModule.actions.expertProfile({commit}).then(() => {
      expect(commit).toHaveBeenCalledTimes(4)
      done()
    })
  })
  it('test for the expert profile state getter', () => {
    const state = {getExpertProfile: {},
      ExpertProfileError: null}
    ExpertUserProfileModule.getters.getExpertProfileInformation(state)
    expect(state).toEqual(state)
  })
  it('commits ProfileErrors mutation when the page loads with errors', (done) => {
    let mock = new AxiosMockAdapter(axios)
    const commit = jest.fn()

    mock.onGet(`https://beta.cowsoko.com/api/v1/experts/841`).reply(500, {
    })

    ExpertUserProfileModule.actions.expertProfile({commit}).then(() => {
      expect(commit).toHaveBeenCalledTimes(4)
      done()
    })
  })
})
