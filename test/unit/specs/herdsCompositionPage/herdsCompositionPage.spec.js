import { shallowMount } from '@vue/test-utils'
import HerdsCompositionPage from '@/pages/HerdsCompositionPage/'
import CowsCount from '@/components/ui/CowsCount/'
import HerdsComposition from '@/pages/HerdsCompositionPage/HerdsComposition/'
import FarmPerformance from '@/pages/HerdsCompositionPage/FarmPerformance/'

describe('HerdsCompositionPage', () => {
  let wrapper

  beforeAll(() => {
    wrapper = shallowMount(HerdsCompositionPage)
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render "CowsCount" only on initial rendering', () => {
    expect(wrapper.findAll(CowsCount).length).toBe(1)
    expect(wrapper.findAll(HerdsComposition).length).toBe(0)
    expect(wrapper.findAll(FarmPerformance).length).toBe(0)
  })

  it('should render "HerdsPerformance" only', () => {
    wrapper.find(CowsCount).vm.$emit('submit', { numberOfCows: 10 })
    expect(wrapper.findAll(CowsCount).length).toBe(0)
    expect(wrapper.findAll(HerdsComposition).length).toBe(1)
    expect(wrapper.findAll(FarmPerformance).length).toBe(0)
  })

  it('should render "FarmPerformance" only', () => {
    wrapper.find(HerdsComposition).vm.$emit('submit', { numberOfMilked: 8 })
    expect(wrapper.findAll(CowsCount).length).toBe(0)
    expect(wrapper.findAll(HerdsComposition).length).toBe(0)
    expect(wrapper.findAll(FarmPerformance).length).toBe(1)
  })
})
